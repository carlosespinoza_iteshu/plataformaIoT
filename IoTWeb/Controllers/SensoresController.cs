﻿using CapaDeDatos.Entidades;
using IoTWeb.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IoTWeb.Controllers
{
    public class SensoresController : Controller
    {
        // GET: Sensores
        public ActionResult Index()
        {
            Proyecto p = (Proyecto)Session["Proyecto"];
            return View(Servicios.repositorySensores.Query(s => s.IdProyecto == p.Id));
        }

        public ActionResult Create()
        {
            return View(new Sensor()
            {
                IdProyecto = ((Proyecto)Session["Proyecto"]).Id
            });
        }

        public ActionResult Crear(Sensor s)
        {
            Proyecto p = (Proyecto)Session["Proyecto"];
            s.IdProyecto = p.Id;
            s.IdUsuario = p.IdUsuario;
            Sensor r = Servicios.repositorySensores.Create(s);

            if (r != null)
            {
                ViewBag.Error = "";
                return RedirectToAction("Index", "Sensores");
            }
            else
            {
                ViewBag.Error = "Error al crear el sensor";
                return View("Create");
            }

        }

        public ActionResult Delete(string id)
        {
            ViewBag.Error = Servicios.repositorySensores.Delete(id) ? "" : "Error al eliminar el sensor";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id)
        {
            ViewBag.Error = "";
            return View(Servicios.repositorySensores.SearchById(id));
        }

        public ActionResult Editar(Sensor s)
        {
            if (Servicios.repositorySensores.Update(s) != null)
            {
                ViewBag.Error = "";
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Error= "No se pudo actualizar el sensor";
                return View("Index");
            }
        }

        public ActionResult Details(string id)
        {
            Session["Sensor"] = Servicios.repositorySensores.SearchById(id);
            return RedirectToAction("Index", "Valores");
        }
    }
}