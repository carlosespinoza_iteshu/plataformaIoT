﻿using CapaDeDatos;
using CapaDeDatos.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IoTWeb.Controllers
{
    public class LecturasController : ApiController
    {
        GenericRepository<Usuario> repositoryUsuarios;
        GenericRepository<Lectura> repositoryLecturas;
        public LecturasController()
        {
            repositoryUsuarios = new GenericRepository<Usuario>();
            repositoryLecturas = new GenericRepository<Lectura>();
        }

        //public IEnumerable<Lectura> Get()
        //{
        //    return repositoryLecturas.Read;
        //}

        // POST: api/Lecturas
        public string Post(string user,string pass,string idSensor,float valor)
        {
            Usuario usuario = repositoryUsuarios.Query(u => u.NombreDeUsuario == user && u.Password == pass).SingleOrDefault();
            if (usuario != null)
            {
                Lectura r = repositoryLecturas.Create(new Lectura()
                {
                    FechaHora = DateTime.Now,
                    IdSensor = idSensor,
                    IdUsuario = usuario.Id,
                    Valor = valor
                });
                if (r != null)
                {
                    return "Ok";
                }
                else
                {
                    return "Error";
                }
            }
            else
            {
                //no existe el usuario o no es correcta la contraseña
                return "Usuario o contraseña incorrectos";
            }
        }

    }
}
