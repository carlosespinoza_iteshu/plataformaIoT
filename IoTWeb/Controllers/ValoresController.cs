﻿using CapaDeDatos.Entidades;
using IoTWeb.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IoTWeb.Controllers
{
    public class ValoresController : Controller
    {
        // GET: Valores
        public ActionResult Index()
        {
            Sensor sensor = (Sensor)(Session["Sensor"]);
            return View(Servicios.repositoryLecturas.Query(l=>l.IdSensor==sensor.Id));
        }

    }
}