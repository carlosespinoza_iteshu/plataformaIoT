﻿using CapaDeDatos.Entidades;
using IoTWeb.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IoTWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Plataforma IoT";
            ViewBag.Mensaje = "";
            return View();
        }

        public ActionResult Registrar()
        {
            ViewBag.Mensaje = "";
            return View("Registrar");
        }

        public ActionResult Registro(Usuario user)
        {
            
            if (Servicios.repositoryUsuarios.Create(user) != null)
            {
                ViewBag.Mensaje = "Usuario creado correctamente";
            }
            else
            {
                ViewBag.Mensaje = "Error al crear el usuario";
            }
            return View("Registrar");
        }

        public ActionResult Login(string usuario, string password)
        {
            try
            {
                Usuario u = Servicios.repositoryUsuarios.Query(user => user.NombreDeUsuario == usuario && user.Password == password).SingleOrDefault();
                if (u != null)
                {
                    Session["Usuario"] = u;
                    return RedirectToAction("Index", "Panel");
                }
                else
                {
                    ViewBag.Mensaje = "Usuario y/o contraseña incorrecta";
                    return View("Index");
                }
            }
            catch (Exception)
            {
                ViewBag.Mensaje = "Error, por favor intenta mas tarde";
                return View("Index");
            }
            
        }
    }
}
