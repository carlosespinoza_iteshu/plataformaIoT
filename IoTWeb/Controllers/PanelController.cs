﻿using CapaDeDatos;
using CapaDeDatos.Entidades;
using IoTWeb.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IoTWeb.Controllers
{
    public class PanelController : Controller
    {
        //GenericRepository<Proyecto> repository = new GenericRepository<Proyecto>();
        // GET: Panel
        public ActionResult Index()
        {
            var proyectos = Servicios.repositoryProyectos.Query(u => u.IdUsuario == ((Usuario)Session["Usuario"]).Id);
            return View(proyectos);
        }


        public ActionResult Create()
        {
            Proyecto p = new Proyecto();
            p.IdUsuario = ((Usuario)Session["Usuario"]).Id;
            return View(p);
        }

        public ActionResult Crear(Proyecto proyecto)
        {
            if (Servicios.repositoryProyectos.Create(proyecto) != null)
            {
                return View("Index", Servicios.repositoryProyectos.Query(u => u.IdUsuario == ((Usuario)Session["Usuario"]).Id));
            }
            else
            {
                ViewBag.Mensaje("Error al crear el proyecto");
                return View("Create", ((Usuario)Session["Usuario"]));
            }
        }

        public ActionResult Edit(string id)
        {
            ViewBag.Mensaje = "";
            return View("Edit", Servicios.repositoryProyectos.SearchById(id));
        }

        public ActionResult Editar(Proyecto proyecto)
        {
            if (Servicios.repositoryProyectos.Update(proyecto) != null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Mensaje = "Error al editar el proyecto";
                return View("Edit", proyecto);
            }
        }

        public ActionResult Delete(string id)
        {
            if (!Servicios.repositoryProyectos.Delete(id))
            {
                ViewBag.Mensaje = "No se pudo borrar el Proyecto";
            }
            return RedirectToAction("Index");
        }
        public ActionResult Details(string id)
        {
            Session["Proyecto"] = Servicios.repositoryProyectos.SearchById(id);
            return RedirectToAction("Index", "Sensores");
        }
    }
}