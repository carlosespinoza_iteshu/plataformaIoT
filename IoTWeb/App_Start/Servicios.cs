﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaDeDatos;
using CapaDeDatos.Entidades;

namespace IoTWeb.App_Start
{
    public static class Servicios
    {
        public static GenericRepository<Usuario> repositoryUsuarios = new GenericRepository<Usuario>();
        public static GenericRepository<Proyecto> repositoryProyectos = new GenericRepository<Proyecto>();
        public static GenericRepository<Sensor> repositorySensores = new GenericRepository<Sensor>();
        public static GenericRepository<Lectura> repositoryLecturas = new GenericRepository<Lectura>();
    }
}