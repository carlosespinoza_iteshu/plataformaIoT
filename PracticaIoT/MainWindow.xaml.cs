﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using OpenNETCF.MQTT;

namespace PracticaIoT
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MQTTClient mqtt;
        string topic= "iotMixta/CAEGIoT01";
        DispatcherTimer timer;
        string mensaje="";
        public MainWindow()
        {
            InitializeComponent();
            mqtt = new MQTTClient("broker.hivemq.com");
            mqtt.Connect("ControlProfeCarlos");
            mqtt.Connected += Mqtt_Connected;
            mqtt.MessageReceived += Mqtt_MessageReceived;

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(1000);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (mensaje != "")
            {
                lstEventos.Items.Add($"{DateTime.Now.ToShortTimeString()}: {mensaje}");
                if (mensaje.Contains("F="))
                {
                    string[] mensajes = mensaje.Split('=');
                    lblLuminosidad.Content = mensajes[1];
                }
                mensaje = "";
            }
        }

        private void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            EscribeEvento("MQTT: " + System.Text.Encoding.UTF8.GetString(payload));
        }

        private void Mqtt_Connected(object sender, EventArgs e)
        {
            mqtt.Subscriptions.Add(new Subscription(topic));
            EscribeEvento("MQTT Conectado");
        }

        private void EscribeEvento(string v)
        {
            mensaje = v;
        }

        private void btnObtenerLuminosidad_Click(object sender, RoutedEventArgs e)
        {
            MandaMensaje("?F");
        }

        private void chkPasto_Checked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("R11");
        }

        private void MandaMensaje(string v)
        {
            mqtt.Publish(topic, v, QoS.FireAndForget, false);
        }

        private void chkPasto_Unchecked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("R10");
        }

        private void chkLuzInterna_Checked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("R21");
        }

        private void chkLuzInterna_Unchecked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("R20");
        }

        private void chkLuzExterna_Checked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("R31");
        }

        private void chkLuzExterna_Unchecked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("R30");
        }

        private void chkTinaco_Checked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("R41");
        }

        private void chkTinaco_Unchecked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("R40");
        }

        private void chkBuzzer_Checked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("B1");
        }

        private void chkBuzzer_Unchecked(object sender, RoutedEventArgs e)
        {
            MandaMensaje("B0");
        }
    }
}
