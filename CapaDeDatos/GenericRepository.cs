﻿using CapaDeDatos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace CapaDeDatos
{
    public class GenericRepository<T> where T:BaseDTO
    {
        public MongoClient client;
        readonly IMongoDatabase db;
        public GenericRepository()
        {
            client = new MongoClient("mongodb+srv://SiCESUSER:FQwr18RExTug7qLP@clustersices-wqqpk.gcp.mongodb.net/test?retryWrites=true&w=majority");
            db = client.GetDatabase("SiCES2019");
        }
        private IMongoCollection<T> Collection() => db.GetCollection<T>(typeof(T).Name);
        public IEnumerable<T> Read {
            get
            {
                try
                {
                    IEnumerable<T> datos = Collection().AsQueryable();
                    return datos == null ? throw new Exception("Error en la conexión") : datos;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public T Create(T entidad)
        {
            try
            {
                entidad.Id = Guid.NewGuid().ToString();
               
                    Collection().InsertOne(entidad);
                    return entidad;
               
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public T Update(T entidad)
        {
            try
            {
                    int r = (int)Collection().ReplaceOne(ent => ent.Id == entidad.Id, entidad).ModifiedCount;
                    return r == 1 ? entidad : null;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool Delete(string id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(e => e.Id == id).DeletedCount;
                return r == 1;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public T SearchById(string id)
        {
            return Query(e => e.Id == id).SingleOrDefault();
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado)
        {
            try
            {
                return Read.Where(predicado.Compile());
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
