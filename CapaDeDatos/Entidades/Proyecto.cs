﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos.Entidades
{
    public class Proyecto:BaseDTO
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string IdUsuario { get; set; }
    }
}
