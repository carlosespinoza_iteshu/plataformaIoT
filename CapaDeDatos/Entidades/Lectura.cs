﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos.Entidades
{
    public class Lectura:BaseDTO
    {
        public DateTime FechaHora { get; set; }
        public float Valor { get; set; }
        public string IdSensor { get; set; }
        public string IdProyecto { get; set; }
        public string IdUsuario { get; set; }
    }
}
