﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos.Entidades
{
    public class Sensor:BaseDTO
    {
        public string Nombre { get; set; }
        public string UnidadDeMedida { get; set; }
        public string Ubicacion { get; set; }
        public string IdProyecto { get; set; }
        public string IdUsuario { get; set; }
    }
}
